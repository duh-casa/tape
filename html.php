<?php

class html {

 function __construct($naslov = "") {
?><!DOCTYPE html>
<html>
 <head>
  <meta charset="UTF-8">
  <title><?php $naslov; ?></title>
  <link rel="stylesheet" href="bootstrap.min.css">
  <link rel="stylesheet" href="bootstrap-theme.min.css">
  <script src="jquery-1.11.2.min.js"></script>
 </head>
 <body>
<?php
 }

 function __destruct() { 
?>
  <script src="bootstrap.min.js"></script>
 </body>
</html> 
<?php
 }

}

class table {

 private $stage;

 function __construct() {
  $this->stage = "head";
?>
 <table class="table">
  <thead>
<?php
 }

 function header($celice = array()) {
?>
 <tr>
 <?php
  foreach($celice as $celica)  {
  ?>
   <th><?php echo $celica; ?></th>
  <?php
  }
 ?>
 </tr>
<?php
 }

 function row($celice = array(), $lastnosti = array("color" => "black")) {
  if($this->stage == "head") {
   ?>
   </thead>
   <tbody>   
   <?php
   $this->stage = "body";
  }
?>
 <tr style="color: <?php echo $lastnosti['color']; ?>;">
 <?php
  foreach($celice as $celica)  {
  ?>
   <td><?php echo $celica; ?></td>
  <?php
  }
 ?>
 </tr>
<?php
 }

 function __destruct() { 
  if($this->stage == "head") {
   ?>
   </thead>
   <?php
  }
  if($this->stage == "body") {
   ?>
   </tbody>
   <?php
  }
  if($this->stage == "foot") {
   ?>
   </tfoot>
   <?php
  }
?>
 </table>
<?php
 }

}

?>
