#!/bin/bash
# zbudi tračno enoto
scsitape -f /dev/sg2 rewind
sleep 2
# prevrti kaseto na začetek
scsitape -f /dev/sg2 rewind
# nastavi block size na TAR default
scsitape -f /dev/sg2 setblk 10240
# pojdi do prvega marka
scsitape -f /dev/sg2 fsf 1
# preberi index
scsitape -f /dev/sg2 read 10240 > /tmp/backupInfoRead.tar
# untar
rm -r /tmp/tapeRead
mkdir /tmp/tapeRead
tar -C /tmp/tapeRead -xf /tmp/backupInfoRead.tar
# prikaži vsebino kasete
cat /tmp/tapeRead/tmp/tape/info.txt
