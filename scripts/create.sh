#!/bin/bash
# naredi info file
echo "Preparing backup index"
rm -r /tmp/tape
mkdir /tmp/tape
date > /tmp/tape/info.txt
echo "Skladišče /home/backup" >> /tmp/tape/info.txt
# zataraj datoteke in nato še info file
echo "Preparing backups"
tar -cvf /tmp/backup.tar /home/backup >> /tmp/tape/info.txt
tar -cf /tmp/backupInfo.tar /tmp/tape
# izpiši datoteke za debug
echo "Files to be written to tape"
cat /tmp/tape/info.txt
# zbudi tračno enoto
echo "Initializing tape unit"
/usr/sbin/scsitape -f /dev/sg2 rewind
sleep 2
# prevrti kaseto na začetek
/usr/sbin/scsitape -f /dev/sg2 rewind
# nastavi block size na TAR default
/usr/sbin/scsitape -f /dev/sg2 setblk 10240
# vstavi prvi mark
/usr/sbin/scsitape -f /dev/sg2 mark 1
# zapiši info file
echo "Writing backup index to tape"
cat /tmp/backupInfo.tar | /usr/sbin/scsitape -f /dev/sg2 write 10240
/usr/sbin/scsitape -f /dev/sg2 mark 1
# zapiši backup file
echo "Writing backup to tape"
cat /tmp/backup.tar | /usr/sbin/scsitape -f /dev/sg2 write 10240 2>&1 | wc -l
/usr/sbin/scsitape -f /dev/sg2 mark 1
# prevrti kaseto na začetek
echo "Finalizing"
/usr/sbin/scsitape -f /dev/sg2 rewind
echo "Done!"
