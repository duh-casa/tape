#!/bin/bash
# zbudi tračno enoto
echo "Initializing tape unit"
scsitape -f /dev/sg2 rewind
sleep 2
# prevrti kaseto na začetek
scsitape -f /dev/sg2 rewind
# nastavi block size na TAR default
scsitape -f /dev/sg2 setblk 10240
# pojdi do drugega marka
echo "Seeking to backup"
scsitape -f /dev/sg2 fsf 2
# preberi index
echo "Restoring backup to disk and preparing recovery folder"
rm -r /home/restore/home/backup/* &
scsitape -f /dev/sg2 read 10240 > /tmp/restore.tar
# untar
echo "Unpacking backup and rewinding tape"
scsitape -f /dev/sg2 rewind &
tar -C /home/restore -xf /tmp/restore.tar
