<?php

require_once "html.php";
$doc = new html("Nadzor nad tračno enoto");

require_once "tape.php";
$tape = new tape();

/*
 
 TODO:
  * vmesnik za preveriti kaj je na kaseti (index)
  * vmesnik za prikaz kaj je na kaseti (prikaz indexa)
  * vmesnik za sprožiti restore v mapo

*/

?>
<h1>Trenutna kaseta</h1>
<p><?php echo $tape->id; ?></p>
<form action="restore.php" method="POST">
 <input type="submit" class="btn btn-warning" value="Povrni backup z kasete">
</form>
